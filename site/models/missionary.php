<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

/**
 * Missionary Model
 */
class jMissionDirectoryModelMissionary extends JModelItem
{
	/**
	 * @var array $missionary
	 */

    protected $missionary;

    /**
     * Method to auto-populate the model state.
     *
     * This method should only be called once per instantiation and is designed
     * to be called on the first call to the getState() method unless the model
     * configuration flag to ignore the request is set.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @return      void
     * @since       2.5
     */
     protected function populateState() 
     {
        $app = JFactory::getApplication();
        // Get the message id
        $input = JFactory::getApplication()->input;
        $id = $input->getInt('id');
        $this->setState('message.id', $id);

        // Load the parameters.
        $params = $app->getParams();
        $this->setState('params', $params);
        parent::populateState();
     }


	/**
	 * Returns a reference to the a Table object, always creating it.
	 * @param       type    The table type to instantiate
	 * @param       string  A prefix for the table class name. Optional.
	 * @param       array   Configuration array for model. Optional.
	 * @return      JTable  A database object
	 * @since       2.5
	 */
	public function getTable($type = 'Missionary',
			$prefix = 'JMissionDirectoryTable',
			$config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
 
    /**
     * Get the missionary
     * @return object The message to be displayed to the user
     */
    public function getMissionary() 
    {
        if (!isset($this->missionary)) 
        {
		$jinput = JFactory::getApplication()->input;
		$alias = $jinput->get('alias', 1, 'text' );
            //$alias = $this->getState('message.alias');
            //die('alias: '.$alias.' id: '.$id);
            $this->_db->setQuery($this->_db->getQuery(true)
                      ->from('#__jmissiondirectory_missionary as missionary')
                      ->leftJoin('#__categories as category ON missionary.category_id=category.id')
                      ->select('missionary.*, category.title as category, category.alias as catalias')
                      ->where('missionary.published=1 and missionary.alias like ' . $this->_db->Quote($alias)));
            //die('where '.'missionary.alias=' . $this->_db->Quote($alias));
            if (!$this->missionary = $this->_db->loadObject()) 
            {
                if ($this->_db->getErrorMsg()) $this->setError($this->_db->getErrorMsg());
             }
             else
             {
                $ids = $this->missionary->category_ids;
                $cat = $this->missionary->category_id;
                if ($ids)
                {
                  $this->_db->setQuery($this->_db->getQuery(true)
                     ->from('#__categories c')
                     ->select('c.id,c.title,c.alias')
                     ->where('c.extension="com_jmissiondirectory" and c.id in('.$ids.') and c.id <> '.$cat));

                  if (!$this->missionary->categories = $this->_db->LoadObjectList())
                  {
                    if ($this->_db->getErrorMsg())
                    {
                        die($this->_db->getErrorMsg());
                        $this->setError($this->_db->getErrorMsg());
                    }
                  }
                } else {
                  $this->missionary->categories = array();
                }

                // Load the JSON string
                $params = new JRegistry;
                // loadJSON is @deprecated    12.1  Use loadString passing JSON as the format instead.
                //$params->loadString($this->missionary->params, 'JSON');
                $params->loadJSON($this->missionary->params);
                $this->missionary->params = $params;
                // Merge global params with missionary params
                $params = clone $this->getState('params');
                $params->merge($this->missionary->params);
                $this->missionary->params = $params;
             }
        }
        return $this->missionary;
    }

	/**
	 * Get the display data
	 */
	public function getDisplayData()
	{
		//request the selected id
		$jinput = JFactory::getApplication()->input;
		$id = $jinput->get('id', 1, 'INT' );
		
		$data = array();
		$raw_data = $this->getData($id);

		$data = array('id'=>$raw_data['id'],
				'displayname'=>$raw_data['displayname'],
				'subtitle'=>$raw_data['subtitle']);
		return $data;
	}

    private function getData($id=1)
    {
    	if (!is_array($this->missionaries)) $this->missionaries = array();
    	if (!isset($this->missionaries[$id]))
    	{
    		$table = $this->getTable();
    		$table->load($id);
    		$this->missionaries[$id] = array('id'=>$table->id,
    									   'displayname'=>$table->displayname,
    									   'subtitle'=>$table->subtitle);
    	}
    	return $this->missionaries[$id];
    }
}