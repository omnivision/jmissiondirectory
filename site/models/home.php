<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

/**
 * Home Model
 */
class jMissionDirectoryModelHome extends JModelItem
{
	/**
	 * @var array $categories
	 * @var array $missionaries
	 */
	
	protected $missionaries;
	protected $categories;
 
    /**
     * Returns a reference to the a Table object, always creating it.
     * @param       type    The table type to instantiate
     * @param       string  A prefix for the table class name. Optional.
     * @param       array   Configuration array for model. Optional.
     * @return      JTable  A database object
     * @since       2.5
     */
    public function getTable($type = 'Category', 
    		$prefix = 'JMissionDirectoryTable', 
    		$config = array()) 
    {
       return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * This method should only be called once per instantiation and is designed
     * to be called on the first call to the getState() method unless the model
     * configuration flag to ignore the request is set.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @return      void
     * @since       2.5
     */
    protected function populateState() 
    {
        $app = JFactory::getApplication();
        $input = JFactory::getApplication()->input;

        // Load the parameters.
        $params = $app->getParams();
        $intro = $input->get('directory_intro_paragraph');
        $heading = $input->get('heading');
        if ($intro) $params->set('directory_intro_paragraph',$intro);
        if ($heading) $params->set('heading',$heading);
        $this->setState('params', $params);
        parent::populateState();
    }

	/**
	 * Get the front page missionaries
	 */
	public function getMissionaries()
	{
		$id = $this->category->id;
		$this->_db->setQuery($this->_db->getQuery(true)
				->from('#__jmissiondirectory_missionary m')
				->select('m.name, m.subtitle, m.teaser, m.picture, c.alias as catalias, m.alias as alias')
				->where('m.front_page=1 and m.published=1')
				->leftJoin('#__categories c on c.id = m.category_id')
				);

		if (!$this->missionaries = $this->_db->LoadObjectList())
		{
			$this->setError($this->_db->getError());
		}
		
		return $this->missionaries;
	}
	
	/**
	 * Get the top level categories
	 */
	public function getCategories()
	{
		$id = $this->category->id;
		$this->_db->setQuery($this->_db->getQuery(true)
				->from('#__categories c')
				->select('c.id,c.title,c.alias,count(distinct(m1.id)) as count')
				->where('c.extension="com_jmissiondirectory" and c.parent_id=1')
				->leftJoin('#__categories child on child.parent_id = c.id')
				->leftJoin('#__jmissiondirectory_missionary m1 on '.
						'm1.published=1 AND('.
						'm1.category_id = c.id'.
						' or m1.category_ids = c.id'.
						' or m1.category_ids like concat("%,",c.id,",%")'.
						' or m1.category_ids like concat(c.id,",%")'.
						' or m1.category_ids like concat("%,",c.id)'.
						')')
				->leftJoin('#__jmissiondirectory_missionary m2 on '.
						'm2.published=1 AND('.
						'm2.category_id = c.id or m2.category_id = child.id'.
						' or m2.category_ids = c.id or m2.category_ids = child.id'.
						' or m2.category_ids like concat("%,",c.id,",%")'.
						' or m2.category_ids like concat("%,",child.id,",%")'.
						' or m2.category_ids like concat(c.id,",%")'.
						' or m2.category_ids like concat(child.id,",%")'.
						' or m2.category_ids like concat("%,",c.id)'.
						' or m2.category_ids like concat("%,",child.id)'.
						')')
				->group('c.id')
				->having('count(distinct(m2.id))')
				);

		if (!$this->categories = $this->_db->LoadObjectList())
		{
			if ($this->_db->getErrorMsg()) die($this->_db->getErrorMsg());
			$this->setError($this->_db->getErrorMsg());
		}
		
		return $this->categories;
	}
	/**
	 * Get the next level categories
	 */
	public function getChildren($id)
	{
		$this->_db->setQuery($this->_db->getQuery(true)
				->from('#__categories c')
				->leftJoin('#__jmissiondirectory_missionary m on '.
						'm.published=1 AND('.
						'm.category_id = c.id '.
						' or category_ids =c.id '.
						' or category_ids like concat("%,",c.id,",%")'.
						' or category_ids like concat(c.id,",%")'.
						' or category_ids like concat("%,",c.id)'.
						')')
				->select('c.id,c.title,c.alias,count(m.id) as count')
				->where('c.parent_id=' . (int)$id)
				->group('c.id')
				->having('count(m.id)')
				);

		if (!$children = $this->_db->LoadObjectList())
		{
			$this->setError($this->_db->getErrorMsg());
		}
		return $children;
	}
}