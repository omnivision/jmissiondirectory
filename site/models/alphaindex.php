<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

/**
 * Alphabetical Index Model
 */
class jMissionDirectoryModelAlphaindex extends JModelItem
{
	/**
	 * @var array $missionaries
	 */
	
	protected $letter;
	protected $missionaries;
 
    /**
     * Returns a reference to the a Table object, always creating it.
     * @param       type    The table type to instantiate
     * @param       string  A prefix for the table class name. Optional.
     * @param       array   Configuration array for model. Optional.
     * @return      JTable  A database object
     * @since       2.5
     */
    public function getTable($type = 'Category', 
    		$prefix = 'JMissionDirectoryTable', 
    		$config = array()) 
    {
       return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * This method should only be called once per instantiation and is designed
     * to be called on the first call to the getState() method unless the model
     * configuration flag to ignore the request is set.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @return      void
     * @since       2.5
     */
    protected function populateState()
    {
    	$app = JFactory::getApplication();
    	$input = JFactory::getApplication()->input;

    	// Load the parameters.
    	$params = $app->getParams();
    	$heading = $input->get('heading');
    	if ($heading) $params->set('heading',$heading);
    	$this->setState('params', $params);
    	parent::populateState();
    }

	/**
	 * Get the missionaries starting with this letter
	 */
	public function getLetter()
	{
		//request the selected id
		$jinput = JFactory::getApplication()->input;
		$this->letter = $jinput->get('letter', 1, 'text' );
		
		return $this->letter;
	}
	
	public function getMissionaries()
	{
		//request the selected letter
		$letter = $this->letter;		
		$this->_db->setQuery($this->_db->getQuery(true)
				->from('#__jmissiondirectory_missionary m')
				->select('m.name, m.subtitle, m.teaser, m.picture, c.alias as catalias, m.alias as alias')
				->leftJoin('#__categories c on c.id = m.category_id')
				->where('m.published=1 AND m.name like "'.$letter.'%"')
				);
		;
		if (!$this->missionaries = $this->_db->LoadObjectList())
		{
			if ($this->_db->getErrorMsg()) $this->setError($this->_db->getErrorMsg());
		}
		return $this->missionaries;
	}
	
}