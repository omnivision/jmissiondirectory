<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

/**
 * Missionary Model
 */
class jMissionDirectoryModelCategory extends JModelItem
{
	/**
	 * @var array $categories
	 */
	
	protected $category;
	protected $missionaries;
	protected $children;
 
    /**
     * Returns a reference to the a Table object, always creating it.
     * @param       type    The table type to instantiate
     * @param       string  A prefix for the table class name. Optional.
     * @param       array   Configuration array for model. Optional.
     * @return      JTable  A database object
     * @since       2.5
     */
    public function getTable($type = 'Category', 
    		$prefix = 'JMissionDirectoryTable', 
    		$config = array()) 
    {
       return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * This method should only be called once per instantiation and is designed
     * to be called on the first call to the getState() method unless the model
     * configuration flag to ignore the request is set.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @return      void
     * @since       2.5
     */
     protected function populateState() 
     {
        $app = JFactory::getApplication();
        // Load the parameters.
        $params = $app->getParams();
        $this->setState('params', $params);
        parent::populateState();
     }
		
	/**
	 * Get the display data
	 */
	public function getDisplayData()
	{
		//request the selected id
		$jinput = JFactory::getApplication()->input;
		$alias = $jinput->get('alias', 1, 'text' );
		
		if ((int)$alias > 1)
		{
			$this->_db->setQuery($this->_db->getQuery(true)
				->from('#__categories as category')
				->select('category.id,category.title,parent.alias as parentalias,parent.title as parent')
				->leftJoin('#__categories as parent on parent.id = category.parent_id')
				->where('category.id=' . $this->_db->Quote((int)$alias)));
		}
		else
		{
			$this->_db->setQuery($this->_db->getQuery(true)
					->from('#__categories as category')
					->select('category.id,category.title,parent.alias as parentalias,parent.title as parent')
					->leftJoin('#__categories as parent on (parent.id = category.parent_id and parent.id > 1)')
					->where('category.alias=' . $this->_db->Quote($alias)));
		}

		if (!$this->category = $this->_db->loadObject())
		{
			$this->setError($this->_db->getError());
		}

        // Load the JSON string
        $params = new JRegistry;
        // loadJSON is @deprecated    12.1  Use loadString passing JSON as the format instead.
        //$params->loadString($this->missionary->params, 'JSON');
        $params->loadJSON($this->category->params);
        $this->category->params = $params;
        // Merge global params with category params
        $params = clone $this->getState('params');
        $params->merge($this->category->params);
        $this->category->params = $params;
		
		return $this->category;
	}
	
	/**
	 * Get the display data
	 */
	public function getMissionaries()
	{
		$id = $this->category->id;
		$this->_db->setQuery($this->_db->getQuery(true)
				->from('#__jmissiondirectory_missionary m')
				->select('m.name, m.subtitle, m.teaser, m.picture, c.alias as catalias, m.alias as alias')
				->where('m.published=1 AND (category_id=' . (int)$id.
				' or category_ids ='.(int)$id.
				' or category_ids like "%,'.(int)$id.',%"'.
				' or category_ids like "'.(int)$id.',%"'.
				' or category_ids like "%,'.(int)$id.'")')
				->leftJoin('#__categories c on c.id = m.category_id')
				);

		if (!$this->missionaries = $this->_db->LoadObjectList())
		{
			$this->setError($this->_db->getError());
		}
		
		return $this->missionaries;
	}
	
	/**
	 * Get the display data
	 */
	public function getChildren()
	{
		$id = $this->category->id;
		$this->_db->setQuery($this->_db->getQuery(true)
				->from('#__categories c')
				->leftJoin('#__jmissiondirectory_missionary m on '.
						'm.published=1 AND('.
						'm.category_id = c.id '.
						' or category_ids =c.id '.
						' or category_ids like concat("%,",c.id,",%")'.
						' or category_ids like concat(c.id,",%")'.
						' or category_ids like concat("%,",c.id)'.
						')')
				->select('c.id,c.title,c.alias,count(m.id) as count')
				->where('c.parent_id=' . (int)$id)
				->group('c.id')
				->having('count(m.id)'));

		if (!$this->children = $this->_db->LoadObjectList())
		{
			$this->setError($this->_db->getErrorMsg());
		}
		if ($this->children[0]->id == null)
			return null;
		
		return $this->children;
	}
}