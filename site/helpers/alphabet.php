<?php
abstract class AlphabetHelper {
	
	public static function getLinks()
	{
		$alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M',
				'N','O','P','Q','R','S','T','U','V','X','Y','Z');
		foreach ($alphabet as $letter)
		{
			$count = AlphabetHelper::getCountForLetter($letter);
			$links[$letter] = $count;
		}
		return $links;
	}
	
	protected static function getCountForLetter($letter='A')
	{
		$db = JFactory::getDBO();
		$db->setQuery($db->getQuery(true)
				->from('#__jmissiondirectory_missionary as missionary')
				->select('count(missionary.id) as count')
				->where('missionary.published=1 AND missionary.name like "' . $letter . '%"'));
		if (!$count = $db->loadObject())
		{
			if ($db->getErrorMsg()) die($db->getErrorMsg());
		}
		else
		{
			return $count->count;
		}
	}
}