<?php
function JMissionDirectoryBuildRoute( &$query )
{
	$segments = array();
	if(isset($query['letter']))
	{
		$segments[] = 'alphaindex';
		$segments[] = $query['letter'];
		unset($query['letter']);
		unset($query['view']);
	}
	if(isset($query['catalias']))
	{
		$segments[] = $query['catalias'];
		unset( $query['catalias'] );
	}
	if(isset($query['alias']))
	{
		$segments[] = $query['alias'];
		unset( $query['alias'] );
	};
	return $segments;
}

function JMissionDirectoryParseRoute( $segments )
{
       $vars = array();
       $app = JFactory::getApplication();
       $menu = $app->getMenu();
       $item = $menu->getActive();
       // Count segments
       $count = count( $segments );

       if ($segments[0]=='alphaindex')
       {
           $vars['view']='alphaindex';
           $vars['letter']=$segments[1];
           return $vars;
       }

       //Handle View and Identifier
       switch( $item->query['view'] )
       {
               case 'home':
                       if($count == 1) {
                               $vars['view'] = 'category';
                       }
                       if($count == 2) {
                               $vars['view'] = 'missionary';
                       }
                       $alias = $segments[$count-1];
                       $alias = str_replace(':','-',$alias);
                       $vars['alias'] = $alias;
                       break;
               case 'category': case 'alphaindex':
                       $alias = $segments[$count-1];
                       $alias = str_replace(':','-',$alias);
                       $vars['alias'] = $alias;
                       $vars['view'] = 'missionary';
                       break;
       }
       return $vars;
}