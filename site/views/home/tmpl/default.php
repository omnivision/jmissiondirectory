<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<h2><?php echo $this->params->get('heading'); ?></h2>
<p><?php echo $this->params->get('directory_intro_paragraph'); ?></p>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <?php $alphabet = AlphabetHelper::getLinks();
$i=1;
foreach ($alphabet as $letter=>$count): 
if ($i>1): ?> | <?php endif; 
if ($count>0): ?> <a href="index.php?view=alphaindex&letter=<?php echo $letter?>"><?php echo $letter?></a>
<?php else: echo $letter?> <?php endif; $i++; endforeach; ?>

<ul id="categories">
<?php foreach ($this->categories as $category): ?>
<li class="toplevel"><a class="toplevel" href="<?php echo 'index.php?alias='.$category->alias; ?>"><?php echo $category->title; ?> (<?php echo $category->count; ?>)</a><ul>
<?php $children = $this->getChildren($category->id);
foreach ($children as $child): ?>
<li><a href="<?php echo 'index.php?alias='.$child->alias; ?>"><?php echo $child->title; ?> (<?php echo $child->count; ?>)</a></li>
<?php endforeach ?></ul></li>
<?php endforeach ?>
</ul>

<?php if (count($this->missionaries)): ?>
<hr>
<h3><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARIES_WORD')?></h3>
<?php echo $this->loadTemplate('missionaries');?>
<?php endif; ?>
<style>
ul#categories li.toplevel
{ display:inline-block; width: auto; min-width:100px; vertical-align:top; padding:10px;}
ul#categories li.toplevel a.toplevel
{ border: 1px solid black; padding:5px; width:100%; display:block; }
</style>