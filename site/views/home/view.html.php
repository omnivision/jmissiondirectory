<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the Home view
*/
class jMissionDirectoryViewHome extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		// Assign data to the view
		$this->categories = $this->get('Categories');
		$this->missionaries = $this->get('Missionaries');
		$this->params = clone $this->getModel()->getState('params');

		Jview::loadHelper('alphabet');

		// Display the view
		parent::display($tpl);
	}
	
	function getChildren($id)
	{
		return $this->getModel()->getChildren($id);
	}
}