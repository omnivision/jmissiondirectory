<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<?php $alphabet = AlphabetHelper::getLinks();
foreach ($alphabet as $letter=>$count): 
if ($count>0): ?> | <a href="index.php?view=alphaindex&letter=<?php echo $letter?>"><?php echo $letter?></a>
<?php else: ?> | <?php echo $letter?> <?php endif; endforeach; ?></p>
<?php if ($this->data->id): ?>
<img style="float:right; max-width:40%; max-height:400px; padding:8px" src="<?php echo $this->data->picture; ?>" />
<h1><?php echo $this->data->displayname; ?></h1>
<p><?php echo $this->data->subtitle; ?></p>
<p><?php echo nl2br($this->data->description); ?></p>
<?php if ($this->data->params->get('show_category')): ?><p><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_CATEGORY_LABEL')?> 
<a href="<?php echo 'index.php?alias='.$this->data->catalias; ?>"><?php echo $this->data->category; ?></a>

<?php foreach ($this->data->categories as $category):?>
 | <a href="index.php?alias=<?php echo $category->alias; ?>"><?php echo $category->title; ?></a>
 <?php endforeach; ?>

</p>
<?php endif ?>
<?php if ($this->data->newsletter): ?>
<h4><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_NEWSLETTER_HEADING')?></h4>
<p><?php echo $this->data->newsletter; ?></p>
<?php endif ?>

<?php if ($this->data->website | $this->data->email | $this->data->phone | $this->data->mobile | $this->data->address | $this->data->city | $this->data->state | $this->data->postcode | $this->data->country): ?>
<h4>Contact Details</h4>
<table>
<?php if ($this->data->website): ?>
 <tr>
  <th><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_FIELD_WEBSITE_LABEL')?></th>
  <td><?php echo $this->data->website; ?></td>
 </tr>
<?php endif; ?>
<?php if ($this->data->email): ?>
 <tr>
  <th><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_FIELD_EMAIL_LABEL')?></th>
  <td><?php echo $this->data->email; ?></td>
 </tr>
<?php endif; ?>
<?php if ($this->data->phone): ?>
 <tr>
  <th><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_FIELD_PHONE_LABEL')?></th>
  <td><?php echo $this->data->phone; ?></td>
 </tr>
<?php endif; ?>
<?php if ($this->data->mobile): ?>
 <tr>
  <th><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_FIELD_MOBILE_LABEL')?></th>
  <td><?php echo $this->data->mobile; ?></td>
 </tr>
<?php endif; ?>
<?php if ($this->data->address): ?>
 <tr>
  <th><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_FIELD_ADDRESS_LABEL')?></th>
  <td><?php echo $this->data->address; ?></td>
 </tr>
<?php endif; ?>
<?php if ($this->data->city): ?>
 <tr>
  <th><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_FIELD_CITY_LABEL')?></th>
  <td><?php echo $this->data->city; ?></td>
 </tr>
<?php endif; ?>
<?php if ($this->data->state): ?>
 <tr>
  <th><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_FIELD_STATE_LABEL')?></th>
  <td><?php echo $this->data->state; ?></td>
 </tr>
<?php endif; ?>
<?php if ($this->data->postcode): ?>
 <tr>
  <th><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_FIELD_POSTCODE_LABEL')?></th>
  <td><?php echo $this->data->postcode; ?></td>
 </tr>
<?php endif; ?>
<?php if ($this->data->country): ?>
 <tr>
  <th><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_FIELD_COUNTRY_LABEL')?></th>
  <td><?php echo $this->data->country; ?></td>
 </tr>
<?php endif; ?>
</table>
<?php endif; ?>
<?php if ($this->data->petracode): ?>
<p/><p><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_PETRACODE_LABEL')?> <?php echo $this->data->petracode; ?></p>

<?php   if ($this->data->params->get('show_gift_options')): ?>
<h3 style='clear: right;'><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_GIFTOPTIONS_HEADING')?> <?php echo $this->data->displayname; ?></h3>
<p><?php echo $this->data->params->get('gift_options_description')?></p>
<div style="display: table;">
<?php     if ($this->data->params->get('show_gift_option_download') & $this->data->params->get('form_file')!=-1): ?>
<div style="width: 210px; display: table-cell; vertical-align: bottom;">
<p><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_BUTTON_DOWNLOAD_FORMS')?></p>
<a href="<?php echo JURI::base().'images/'.$this->data->params->get('form_file');?>" class="paypal_button">
<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_BUTTON_DOWNLOAD_FORMS')?></a>
</div>
<?php  endif;
 if ($this->data->params->get('show_gift_option_single')): ?>
<div style="width: 210px; display: table-cell;">
	<!-- Paypal Code for once-off Starts -->
	<form action="https://<?php echo $this->data->params->get('paypal_status')?>.paypal.com/cgi-bin/webscr" style="margin:0;" method="post" target="_top">
		<input type="hidden" name="cmd" value="_s-xclick">
		<input type="hidden" name="hosted_button_id" value="<?php echo $this->data->params->get('gift_single_button_id')?>">
		<!-- These options tell the account holder who the money is for -->
		<input type="hidden" name="on0" value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_NAME')?>:">
		<input type="hidden" name="os0" value="<?php echo $this->data->name; ?>">
		<input type="hidden" name="on1" value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_PETRACODE_LABEL')?>">
		<input type="hidden" name="os1" value="<?php echo $this->data->petracode; ?>">
		<table>
			<tr><td><input type="hidden" name="on2" value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_SINGLE_GIFT_OPTIONS')?>:">
				<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_SINGLE_GIFT_OPTIONS')?>:</td></tr><tr><td><select name="os2">
				<option value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_SINGLE_GIFT_OPTION_SUPPORT')?>">
					<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_SINGLE_GIFT_OPTION_SUPPORT')?></option>
				<option value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_SINGLE_GIFT_OPTION_SPECIAL')?>">
					<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_SINGLE_GIFT_OPTION_SPECIAL')?></option>
			</select> </td></tr>
		</table>
		<input type="submit" class="paypal_button" name="submit" alt="PayPal — The safer, easier way to pay online."
		 value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_BUTTON_ONCE_OFF_GIFT')?>">
	</form>
	<!-- Paypal Code for once-off ends -->
</div>
<?php     endif; ?>
<?php     if ($this->data->params->get('show_gift_option_regular')): ?>
<div style="width: 210px; display: table-cell;">
	<!-- Paypal Code for subscription starts -->
	<form action="https://<?php echo $this->data->params->get('paypal_status')?>.paypal.com/cgi-bin/webscr" style="margin:0;" method="post" target="_top">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="<?php echo $this->data->params->get('gift_regular_button_id')?>">
	<table>
	<!-- The following options MUST reflect those set in paypal. -->
	<tr><td><input type="hidden" name="on0" value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_REGULAR_GIFT_OPTIONS')?>:">
	<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_REGULAR_GIFT_OPTIONS')?>:</td></tr><tr><td><select name="os0">
	<option value="<?php echo $this->data->params->get('gift_regular_option1_value')?>"><?php echo $this->data->params->get('gift_regular_option1_text')?></option>
	<option value="<?php echo $this->data->params->get('gift_regular_option2_value')?>"><?php echo $this->data->params->get('gift_regular_option2_text')?></option>
	<option value="<?php echo $this->data->params->get('gift_regular_option3_value')?>"><?php echo $this->data->params->get('gift_regular_option3_text')?></option>
	<option value="<?php echo $this->data->params->get('gift_regular_option4_value')?>"><?php echo $this->data->params->get('gift_regular_option4_text')?></option>
	</select> </td></tr>
	</table>
	<!-- These options tell the account holder who the money is for -->
	<input type="hidden" name="on0" value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_NAME')?>:">
	<input type="hidden" name="os0" value="<?php echo $this->data->name; ?>">
	<input type="hidden" name="on1" value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_PETRACODE_LABEL')?>">
	<input type="hidden" name="os1" value="<?php echo $this->data->petracode; ?>">
	<input type="submit" class="paypal_button" name="submit" alt="PayPal — The safer, easier way to pay online."
	 value="<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_BUTTON_REGULAR_GIFT')?>">
	</form>
	<!-- Paypal Code for subscription ends -->	
</div>
<?php     endif; ?>
</div>
<p><?php echo $this->data->params->get('gift_options_footer')?></p>
<?php   endif; ?>
<?php endif ?>
<style>
.paypal_button
{ 
 background:url('<?php echo JURI::base(); ?>media/com_jmissiondirectory/images/button.png') no-repeat; 
 display: block; width:140px!important; height:34px; border: none;  
 font-weight: 500; color: white; text-shadow: 2px 1px black; 
 text-align:center; 
}
</style>
<?php else: ?>
<?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARY_NOTFOUND')?>
<?php endif; ?>