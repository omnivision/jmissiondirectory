<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<h2><?php echo $this->data->params->get('heading'); ?></h2>
<?php 
switch ($this->missionaries_word)
{
	case 3: break;
	case 2: echo '<a href="index.php?view=home">Workers:</a> '; break;
	default: case 1: echo '<a href="index.php?view=home">Missionaries:</a> '; break;
}
if ($this->data->parent): ?><a href="<?php echo 'index.php?alias='.$this->data->parentalias;?>"><?php echo $this->data->parent; ?>: </a><?php endif; ?>
<?php echo $this->data->title; ?>
<p id="alphabet"><?php $alphabet = AlphabetHelper::getLinks();
foreach ($alphabet as $letter=>$count): 
if ($count>0): ?> | <a href="index.php?view=alphaindex&letter=<?php echo$letter?>"><?php echo$letter?></a>
<?php else: ?> | <?php echo$letter?> <?php endif; endforeach; ?></p>

<h1><?php echo $this->data->title; ?></h1>

<ul id="categories">
<?php if (count($this->children)): foreach ($this->children as $child): ?>
<li class="toplevel"><a class="toplevel" href="<?php echo 'index.php?alias='.$child->alias; ?>"><?php echo $child->title; ?> (<?php echo $child->count; ?>)</a></li>
<?php endforeach; endif; ?></ul>

<h2><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARIES_WORD')?></h2>

<?php 
 echo $this->loadTemplate('missionaries');?>

<style>
ul#categories li.toplevel
{ display:inline-block; width: auto; min-width:100px; vertical-align:top; padding:10px;}
ul#categories li.toplevel a.toplevel
{ border: 1px solid black; padding:5px; width:100%; display:block; }
</style>