<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the Category view
*/
class jMissionDirectoryViewCategory extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		// Assign data to the view
		$this->data = $this->get('DisplayData');
		$this->missionaries = $this->get('Missionaries');
		$this->children = $this->get('Children');

		Jview::loadHelper('alphabet');
		
		//Uses JInput if magic quotes is turned off. Falls back to use JRequest.
		if(!get_magic_quotes_gpc) {
			$this->missionaries_word = JFactory::getApplication()->input->get('missionaries_word', 1, 'INT' );
		} else {
			$this->missionaries_word = JRequest::getInt('missionaries_word');
		}

		// Display the view
		parent::display($tpl);
	}
}