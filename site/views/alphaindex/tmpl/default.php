<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<h2><?php echo $this->params->get('heading'); ?></h2>
<a href="index.php?view=home"><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARIES_WORD')?></a> starting with: 
<?php $alphabet = AlphabetHelper::getLinks();
$i=1;
foreach ($alphabet as $letter=>$count): 
if ($i>1): ?> | <?php endif; 
if ($count>0): ?> <a href="index.php?view=alphaindex&letter=<?php echo $letter?>"><?php echo $letter?></a>
<?php else: echo $letter?> <?php endif; $i++; endforeach; ?>


<h2><?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARIES_WORD')?>
 starting with <?php echo $this->letter; ?></h2>
<?php 
 echo $this->loadTemplate('missionaries');?>