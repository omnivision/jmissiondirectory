<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controller library
jimport('joomla.application.component.controller');
 
/**
 * Hello World Component Controller
 */
class jMissionDirectoryController extends JController
{
	function display($cachable=false,$urlparams=false)
	{
		$viewName	= JRequest::getCmd( 'view', 'JMissionDirectory' );
		parent::display();
	}
}