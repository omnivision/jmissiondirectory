<?php
// No direct access to this file
defined('_JEXEC') or die;
 
/**
 * JMissionDirectory component helper.
 */
abstract class JMissionDirectoryHelper
{
        /**
         * Configure the Linkbar.
         */
        public static function addSubmenu($submenu) 
        {
                JSubMenuHelper::addEntry(JText::_('COM_JMISSIONARYDIRECTORY_SUBMENU_MISSIONARIES'),
                                         'index.php?option=com_jmissiondirectory', $submenu == 'messages');
                JSubMenuHelper::addEntry(JText::_('COM_JMISSIONARYDIRECTORY_SUBMENU_CATEGORIES'),
                                         'index.php?option=com_categories&view=categories&extension=com_jmissiondirectory',
                                         $submenu == 'categories');
                // set some global property
                $document = JFactory::getDocument();
                //$document->addStyleDeclaration('.icon-48-jmissiondirectory ' .
                 //                              '{background-image: url(../media/com_jmissiondirectory/images/tux-48x48.png);}');
                if ($submenu == 'categories') 
                {
                        $document->setTitle(JText::_('COM_JMISSIONARYDIRECTORY_ADMINISTRATION_CATEGORIES'));
                }
        }
 
        /**
         * Get the actions
         */
        public static function getActions($messageId = 0)
        {       
                jimport('joomla.access.access');
                $user   = JFactory::getUser();
                $result = new JObject;
 
                if (empty($messageId)) {
                        $assetName = 'com_jmissiondirectory';
                }
                else {
                        $assetName = 'com_jmissiondirectory.message.'.(int) $messageId;
                }
 
                $actions = JAccess::getActions('com_jmissiondirectory', 'component');
 
                foreach ($actions as $action) {
                        $result->set($action->name, $user->authorise($action->name, $assetName));
                }
 
                return $result;
        }
}