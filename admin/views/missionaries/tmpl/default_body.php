<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): 
		$published 	= JHTML::_('grid.published', $item, $i );
		$front_page	= JHTML::_('grid.boolean', $i, $item->front_page,'frontPage','unFrontPage');?>
        <tr class="row<?php echo $i % 2; ?>">
                <td>
                        <?php echo $item->id; ?>
                </td>
                <td>
                        <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                </td>
                <td>
                        <?php echo $item->name; ?>
                </td>
                <td>
                        <?php echo $item->subtitle; ?>
                </td>
                <td>
                        <?php echo $item->category; ?>
                </td>
               <td align="center">
                        <?php echo $published;?>
               </td>
               <td align="center">
                        <?php echo $front_page;?>
               </td>
        </tr>
<?php endforeach; ?>