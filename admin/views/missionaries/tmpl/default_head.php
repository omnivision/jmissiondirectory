<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<tr>
        <th width="5">
                <?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARIES_HEADING_ID'); ?>
        </th>
        <th width="20">
                <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->items); ?>);" />
        </th>
        <th>
                <?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARIES_HEADING_NAME'); ?>
        </th>
        <th>
                <?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARIES_HEADING_SUBTITLE'); ?>
        </th>
        <th>
                <?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARIES_HEADING_CATEGORY'); ?>
        </th>
        <th width="5%" nowrap="nowrap">
                <?php echo JText::_( 'Published' ); ?>
        </th>
        <th>
                <?php echo JText::_('COM_JMISSIONDIRECTORY_MISSIONARIES_HEADING_FRONT_PAGE'); ?>
        </th>
</tr>