<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * Missionaries View
 */
class JMissionDirectoryViewMissionaries extends JView
{
    protected $canDo;
        /**
         * Missionaries view display method
         * @return void
         */
        function display($tpl = null) 
        {
                // Get data from the model
                $items = $this->get('Items');
                $pagination = $this->get('Pagination');

                // What Access Permissions does this user have? What can (s)he do?
                $this->canDo = JMissionDirectoryHelper::getActions();
 
                // Check for errors.
                if (count($errors = $this->get('Errors'))) 
                {
                        JError::raiseError(500, implode('<br />', $errors));
                        return false;
                }
                // Assign data to the view
                $this->items = $items;
                $this->pagination = $pagination;

                // Set the toolbar
                $this->addToolBar();

                // Display the template
                parent::display($tpl);
        }

        protected function addToolBar($total=null)
        {
            JToolBarHelper::title(JText::_('COM_JMISSIONDIRECTORY_MANAGER_MISSIONARIES').
                 //Reflect number of items in title!
                ($total?' <span style="font-size: 0.5em; vertical-align: middle;">('.$total.')</span>':'')
                , 'jmissiondirectory');
            if ($this->canDo->get('core.delete'))
            {
                JToolBarHelper::deleteList('','missionaries.delete');
            }
            if ($this->canDo->get('core.edit'))
            {
                JToolBarHelper::editList('missionary.edit');
                JToolBarHelper::publishList();
                JToolBarHelper::unpublishList();
            }
            if ($this->canDo->get('core.create'))
            {
                JToolBarHelper::addNew('missionary.add');
            }
            if ($this->canDo->get('core.admin'))
            {
                JToolBarHelper::divider();
                JToolBarHelper::preferences('com_jmissiondirectory');
            }
        }
        /**
         * Method to set up the document properties
         *
         * @return void
         */
        protected function setDocument() 
        {
                $document = JFactory::getDocument();
                $document->setTitle(JText::_('COM_JMISSIONDIRECTORY_ADMINISTRATION'));
        }
}