ALTER TABLE `#__jmissiondirectory_missionary` ADD `picture` varchar(255) AFTER `displayname`;
ALTER TABLE `#__jmissiondirectory_missionary` ADD `description` text AFTER `picture`;
ALTER TABLE `#__jmissiondirectory_missionary` ADD `newsletter` text AFTER `description`;
ALTER TABLE `#__jmissiondirectory_missionary` ADD `petracode` varchar(255) AFTER `category_ids`;