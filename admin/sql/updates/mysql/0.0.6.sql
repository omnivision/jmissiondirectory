DROP TABLE IF EXISTS `#__jmissiondirectory_category`;

CREATE TABLE `#__jmissiondirectory_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent` int(11) NULL,
  PRIMARY KEY(`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
  
INSERT INTO `#__jmissiondirectory_category` (`name`,`parent`) VALUES
  ('Home State',NULL), ('Ships',NULL), ('Africa',NULL), ('Asia',NULL),
  ('NSW',1), ('VIC',1), ('Logos Hope',2), ('South Africa',3), ('China',4), ('Thailand',4);
  
DROP TABLE IF EXISTS `#__jmissiondirectory_missionary`;

CREATE TABLE `#__jmissiondirectory_missionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `subtitle` varchar(255),
  `teaser` text,
  `displayname` varchar(255) NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
  
INSERT INTO `#__jmissiondirectory_missionary` (`name`,`subtitle`,`displayname`) VALUES
  ('Bloggs, Joe','National worker','Joe Bloggs'),
  ('Smith Family','Serving Jesus','Smith Family');