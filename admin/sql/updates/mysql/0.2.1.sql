ALTER TABLE `#__jmissiondirectory_missionary` ADD `published` int(1) default 0 AFTER `alias`;
UPDATE `#__jmissiondirectory_missionary` SET `published` = 1;