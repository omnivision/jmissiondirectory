DROP TABLE IF EXISTS `#__jmissiondirectory_missionary`;

CREATE TABLE `#__jmissiondirectory_missionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` INT(10) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `published` int(1) default 0,
  `front_page` int(1) default 0,
  `subtitle` varchar(255),
  `teaser` text,
  `displayname` varchar(255) NOT NULL,
  `picture` varchar(255),
  `description` text,
  `newsletter` text,
  `website` varchar(255),
  `email` varchar(255),
  `phone` varchar(255),
  `mobile` varchar(255),
  `address` varchar(255),
  `city` varchar(255),
  `state` varchar(255),
  `postcode` varchar(255),
  `country` varchar(255),
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_ids` varchar(255),
  `petracode` varchar(255),
  `params` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY(`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
  
INSERT INTO `#__jmissiondirectory_missionary` (`name`,`alias`,`published`,`subtitle`,`displayname`,`category_id`) VALUES
  ('Bloggs, Joe','bloggs-joe',1,'National worker','Joe Bloggs',4),
  ('Smith Family','smith-family',1,'Serving Jesus','Smith Family',5);