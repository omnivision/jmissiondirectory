<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');
/**
 * Missionaries List Model
*/
class JMissionDirectoryModelMissionaries extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select('missionary.id as id,missionary.name as name, missionary.published, missionary.front_page, subtitle,category.title as category, category_id');
		// From the hello table
		$query->from('#__jmissiondirectory_missionary missionary');
		$query->leftJoin('#__categories category on category.id = category_id');
		return $query;
	}

    /**
	* Set the published value of the given missionary
	*/
    function setItemState( $items, $state )
    {
        if(is_array($items))
        {
            foreach ($items as $id)
            {
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->update('#__jmissiondirectory_missionary')
                      ->set('`published`='.$state)
                      ->where('`id`='.$id);
                $db->setQuery($query);
                $db->execute();
            }
        }
    }

    /**
	* Set the published value of the given missionary
	*/
    function setItemFrontPage( $items, $state )
    {
        if(is_array($items))
        {
            foreach ($items as $id)
            {
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->update('#__jmissiondirectory_missionary')
                      ->set('`front_page`='.$state)
                      ->where('`id`='.$id);
                $db->setQuery($query);
                $db->execute();
            }
        }
    }
}