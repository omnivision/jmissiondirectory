<?php
// No direct access to this file
defined('_JEXEC') or die;
 
// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');
 
/**
 * Missionary Form Field class for the JMissionDirectory component
 */
class JFormFieldMissionary extends JFormFieldList
{
        /**
         * The field type.
         *
         * @var         string
         */
        protected $type = 'missionary';
 
        /**
         * Method to get a list of options for a list input.
         *
         * @return      array           An array of JHtml options.
         */
        protected function getOptions() 
        {
                $db = JFactory::getDBO();
                $query = $db->getQuery(true);
                $query->select('missionary.id as id, missionary.name as name, category.title as category, category_id');
                $query->from('#__jmissiondirectory_missionary missionary');
                $query->leftJoin('#__categories category on category_id=category.id');
                $db->setQuery((string)$query);
                $missionaries = $db->loadObjectList();
                $options = array();
                if ($missionaries)
                {
                        foreach($missionaries as $missionary) 
                        {
                                $options[] = JHtml::_('select.option', $missionary->id, $missionary->name .
                                		($missionary->category_id ? ' ('. $missionary->category . ')' : ''));
                        }
                }
                $options = array_merge(parent::getOptions(), $options);
                return $options;
        }
}