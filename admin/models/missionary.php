<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

/**
 * Missionary Model
 */
class JMissionDirectoryModelMissionary extends JModelAdmin
{
    /**
     * getTable override
     */
    public function getTable($type='Missionary', $prefix='JMissionDirectoryTable', $config= array())
    {
        return JTable::getInstance($type,$prefix,$config);
    }

    /*
     * Method to get the record form
     */
    public function getForm($data = array(), $loadData = true)
    {
        // Get the form
        $form = $this->loadForm('com_jmissiondirectory.missionary','missionary',
                                array('control'=>'jform','load_data'=>$loadData));
        if (empty($form))
        {
            return false;
        }
        return $form;
    }

    /**
     * Method to get the data that should be injected in the form
     */
    protected function loadFormData()
    {
        // Check the se4ssion for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_jmissiondirectory.edit.missionary.data', array());
        if (empty($data))
        {
            $data = $this->getItem();
            $data->category_ids = explode(',', $data->category_ids);
        }
        return $data;
    }
    /**
     * Method to check if it's OK to delete a missionary. Overwrites JModelAdmin::canDelete
     */
    protected function canDelete($record)
    {
        if( !empty( $record->id ) ){
            $user = JFactory::getUser();
            return $user->authorise( "core.delete", "com_jmissiondirectory.missionary." . $record->id );
        }
    }
}