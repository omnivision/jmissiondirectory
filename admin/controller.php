<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controller library
jimport('joomla.application.component.controller');
 
/**
 * General Controller of JMissionDirectory component
 */
class JMissionDirectoryController extends JController
{
        /**
         * display task
         *
         * @return void
         */
        function display($cachable = false, $urparams = false) 
        {
                // set default view if not set
                $input = JFactory::getApplication()->input;
                $input->set('view', $input->getCmd('view', 'Missionaries'));
 
                // call parent behavior
                parent::display($cachable);
 
                // Set the submenu
                JMissionDirectoryHelper::addSubmenu('missionaries');
        }
        /**
         * Set the item to be published
         */
        function publish()
        {
        	// Check for request forgeries
        	JRequest::checkToken() or jexit( 'Invalid Token' );

        	// Get some variables from the request
        	$menutype = JRequest::getVar('menutype', '', 'post', 'menutype');
        	$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
        	JArrayHelper::toInteger($cid);

        	$model = $this->getModel( 'Missionaries' );
        	if ($model->setItemState($cid, 1)) {
        		$msg = JText::sprintf( 'Missionaries Published', count( $cid ) );
        	} else {
        		$msg = $model->getError();
        	}
        	$this->setRedirect('index.php?option=com_jmissiondirectory&view=missionaries' . $menutype, $msg);
        }

        /**
         * Set the item to be not published
         */
        function unpublish()
        {
        	// Check for request forgeries
        	JRequest::checkToken() or jexit( 'Invalid Token' );

        	// Get some variables from the request
        	$menutype = JRequest::getVar('menutype', '', 'post', 'menutype');
        	$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
        	JArrayHelper::toInteger($cid);

        	$model =& $this->getModel( 'Missionaries' );
        	if ($model->setItemState($cid, 0)) {
        		$msg = JText::sprintf( 'Missionaries Unpublished', count( $cid ) );
        	} else {
        		$msg = $model->getError();
        	}
        	$this->setRedirect('index.php?option=com_jmissiondirectory&view=missionaries', $msg);
        }

        /**
         * Set the item to be published
         */
        function frontPage()
        {
        	// Check for request forgeries
        	JRequest::checkToken() or jexit( 'Invalid Token' );

        	// Get some variables from the request
        	$menutype = JRequest::getVar('menutype', '', 'post', 'menutype');
        	$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
        	JArrayHelper::toInteger($cid);

        	$model = $this->getModel( 'Missionaries' );
        	if ($model->setItemFrontPage($cid, 1)) {
        		$msg = JText::sprintf( 'Missionaries put on front page', count( $cid ) );
        	} else {
        		$msg = $model->getError();
        	}
        	$this->setRedirect('index.php?option=com_jmissiondirectory&view=missionaries' . $menutype, $msg);
        }

        /**
         * Set the item to be not published
         */
        function unFrontPage()
        {
        	// Check for request forgeries
        	JRequest::checkToken() or jexit( 'Invalid Token' );

        	// Get some variables from the request
        	$menutype = JRequest::getVar('menutype', '', 'post', 'menutype');
        	$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
        	JArrayHelper::toInteger($cid);

        	$model =& $this->getModel( 'Missionaries' );
        	if ($model->setItemFrontPage($cid, 0)) {
        		$msg = JText::sprintf( 'Missionaries removed from front page', count( $cid ) );
        	} else {
        		$msg = $model->getError();
        	}
        	$this->setRedirect('index.php?option=com_jmissiondirectory&view=missionaries', $msg);
        }
}