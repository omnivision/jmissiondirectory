<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Access check: is this user allowed to access the backend of this component?
if (!JFactory::getUser()->authorise('core.manage', 'com_jmissiondirectory'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
} 
// require helper file
JLoader::register('JMissionDirectoryHelper', dirname(__FILE__) . '/helpers/jmissiondirectory.php');

// import joomla controller library
jimport('joomla.application.component.controller');
 
// Get an instance of the controller prefixed by JMissionDirectory
$controller = JController::getInstance('JMissionDirectory');
 
// Get the task
$jinput = JFactory::getApplication()->input;
$task = $jinput->get('task', "", 'STR' );
 
// Perform the Request task
$controller->execute($task);
 
// Redirect if set by the controller
$controller->redirect();