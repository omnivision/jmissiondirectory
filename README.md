# jMissionDirectory

This Joomla 2.5 Component allows you to store a directory of missionaries with categories and alphabetised.
Each missionary can have two paypal giving buttons. Those buttons need to be defined in your paypal account
and the button_id entered in the preferences.

## Installation

1. Go to the [Downloads](https://bitbucket.org/omnivision/jmissiondirectory/downloads) page and download the latest release
2. In your Joomla Admin go to Extensions -> Extension manager
3. Upload and install the zip file

## Setup

Some setup is required.

### Form

You will need to upload your giving form or zip file of forms and specify it in the options dialog box. 

### Paypal buttons

You will need a paypal account. First get one of those.
Go to Profile, My selling preferences/tools and manage your payment buttons.

Create a new 'buy now' button.
Call it 'Gift to missionary' or something like that. The id is for your benefit.
Don't set a price (unless you want to restrict to one amount to give)
Save it.
All you want from the code screen is the hosted button ID.
```

#!html

<input type="hidden" name="hosted_button_id" value="xxxxxx">

```
Just copy what is in the value field. Put this value in the field "Single gift buttonid" under "Missionaries" in the Options dialog box.

Next create a subscription button. Again the name and ID are your choice.
Do 'add a dropdown menu with prices and options'. Add a fourth option. Note down what you put in these boxes
 - you will need to put them into the appropriate fields in the jMissionDirectory options dialog box.
Now save and copy the hosted button id as before.

Chose 'Live' for 'Paypal status'.

Hopefully that will have you up and running.

## Updating

Joomla will inform you when there is an update available.

## Coding

This component is very simple and is based on the following tutorial. It would be wise to view it before modifying code
http://docs.joomla.org/Developing_a_Model-View-Controller_Component/2.5
I welcome anyone who would like to help with this project - why not start by forking it!
